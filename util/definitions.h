/*
 * definitions.h
 *
 *  Created on: 30-01-2015
 *      Author: Karol Witkowski
 */

#ifndef DEFINITIONS_H_
#define DEFINITIONS_H_

#define PI		3.14159265359

typedef uint8_t u8;
typedef uint16_t u16;

typedef struct {
	u8 hour;
	u8 minute;
	u8 second;
	u8 day;
	u8 month;
	u8 year;
	u8 weekday;
} time_date;

#endif // DEFINITIONS_H_
