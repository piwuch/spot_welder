/*
 * makra.h
 *
 *  Created on: 30-01-2015
 *      Author: Karol Witkowski
 */

#ifndef MAKRA_H_
#define MAKRA_H_

#define SET(reg,val)	reg |= (val)
#define SET2(reg,val1,val2)	reg |= (val1)|(val2)
#define SET3(reg,val1,val2,val3)	reg |= (val1)|(val2)|(val2)
#define SETPIN(reg,val)	PORT(reg) |= _BV(PIN(val))
#define CLR(reg,val)	reg &=~(val)
#define CLRPIN(reg,val)	PORT(reg) &= ~_BV(PIN(val))
#define TOG(reg,val)	reg ^= (val)
#define TOGPIN(reg,val)	PORT(reg) ^= _BV(PIN(val))
#define GET(reg,bit) 	((reg) & _BV(bit))

// Makra upraszczaj�ce dost�p do port�w
// *** PORT
#define PORT(x) XPORT(x)
#define XPORT(x) (PORT##x)
// *** PIN
#define PIN(x) XPIN(x)
#define XPIN(x) (PIN##x)
// *** DDR
#define DDR(x) XDDR(x)
#define XDDR(x) (DDR##x)

#define ROTATE_RIGHT(x) x=(x>>1)|(x<<7)
#define ROTATE_LEFT(x) x=(x<<1)|(x>>7)

#endif // MAKRA_H_
