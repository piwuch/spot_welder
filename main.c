/*
 * main.c
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 24 lis 2019
 *   	    Author: Karol Witkowski
 */
#define FOSC 8000000
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>

#include "util/makra.h"

#include "modules/segment_display.h"



int main(void) {
	segment_display_init();
	u8 counter = 0;
	u8 timer = 0;

	while(1){
		_delay_ms(10);
		segment_display_clear();
		segment_display_set_digit(counter%10);
		segment_display_set_position(0);
		_delay_ms(10);
		segment_display_clear();
		segment_display_set_digit((counter/10)%10);
		segment_display_set_position(1);
		timer++;
		if(timer > 99){
			timer = 0;
			counter++;
		}

	}
}
