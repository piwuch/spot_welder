/*
 * segment_display.h
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 24 lis 2019
 *   	    Author: Karol Witkowski
 */
#ifndef SEGMENT_DISPLAY_H_
#define SEGMENT_DISPLAY_H_

#include <avr/io.h>

#include "../util/definitions.h"
#include "../util/makra.h"

#define SEGMENT_A_PORT 	D
#define SEGMENT_A_PIN 	5
#define SEGMENT_B_PORT 	D
#define SEGMENT_B_PIN 	1
#define SEGMENT_C_PORT 	D
#define SEGMENT_C_PIN 	2
#define SEGMENT_D_PORT 	D
#define SEGMENT_D_PIN 	4
#define SEGMENT_E_PORT 	D
#define SEGMENT_E_PIN 	3
#define SEGMENT_F_PORT 	D
#define SEGMENT_F_PIN 	6
#define SEGMENT_G_PORT 	B
#define SEGMENT_G_PIN 	6
#define DOT_PORT 		B
#define DOT_PIN 		7
#define DIGIT_1_PORT 	D
#define DIGIT_1_PIN 	7
#define DIGIT_0_PORT 	D
#define DIGIT_0_PIN 	0

// Function declarations
void segment_display_init();
void segment_display_set_position(u8);
void segment_display_set_digit(u8);
void segment_display_clear();

#endif /* MODULES_SEGMENT_DISPLAY_H_ */
