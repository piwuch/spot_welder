/*
 * segment_display.c
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 28 lis 2019
 *   	    Author: Karol Witkowski
 */

#include "segment_display.h"

void segment_display_init(){
	DDR(SEGMENT_A_PORT)	|= (1<<SEGMENT_A_PIN);
	DDR(SEGMENT_B_PORT)	|= (1<<SEGMENT_B_PIN);
	DDR(SEGMENT_C_PORT)	|= (1<<SEGMENT_C_PIN);
	DDR(SEGMENT_D_PORT)	|= (1<<SEGMENT_D_PIN);
	DDR(SEGMENT_E_PORT)	|= (1<<SEGMENT_E_PIN);
	DDR(SEGMENT_F_PORT)	|= (1<<SEGMENT_F_PIN);
	DDR(SEGMENT_G_PORT)	|= (1<<SEGMENT_G_PIN);
	DDR(DOT_PORT)		|= (1<<DOT_PIN);
	DDR(DIGIT_1_PORT)	|= (1<<DIGIT_1_PIN);
	DDR(DIGIT_0_PORT)	|= (1<<DIGIT_0_PIN);
	segment_display_clear();
}
void segment_display_set_position(u8 position){
	if(position == 0){
		CLRPIN(DIGIT_0_PORT, DIGIT_0_PIN);
	}
	if(position == 1){
		CLRPIN(DIGIT_1_PORT, DIGIT_1_PIN);
	}
}

void segment_display_set_digit(u8 digit){
	u8 bit1 = digit & 1;
	u8 bit2 = digit & 2;
	u8 bit3 = digit & 4;
	u8 bit4 = digit & 8;
	//A
	if(bit4 || bit2 || (!bit3 && !bit1) || (bit3 && bit1)){
		CLRPIN(SEGMENT_A_PORT, SEGMENT_A_PIN);
	} else {
		SETPIN(SEGMENT_A_PORT, SEGMENT_A_PIN);
	}
	//B
	if(!bit3 || (!bit2 && !bit1) || (bit2 && bit1)){
		CLRPIN(SEGMENT_B_PORT, SEGMENT_B_PIN);
	} else {
		SETPIN(SEGMENT_B_PORT, SEGMENT_B_PIN);
	}
	//C
	if(!bit2 || bit1 || bit3){
		CLRPIN(SEGMENT_C_PORT, SEGMENT_C_PIN);
	} else {
		SETPIN(SEGMENT_C_PORT, SEGMENT_C_PIN);
	}
	//D
	if(bit4 || (!bit3 && !bit1) || (!bit3 && bit2) || (bit2 && !bit1) || (bit3 && !bit2 && bit1)){
		CLRPIN(SEGMENT_D_PORT, SEGMENT_D_PIN);
	} else {
		SETPIN(SEGMENT_D_PORT, SEGMENT_D_PIN);
	}
	//E
	if((!bit3 && !bit1) || (bit2 && !bit1)){
		CLRPIN(SEGMENT_E_PORT, SEGMENT_E_PIN);
	} else {
		SETPIN(SEGMENT_E_PORT, SEGMENT_E_PIN);
	}
	//F
	if(bit4 || (!bit2 && !bit1) || (bit3 && !bit2) || (bit3 && !bit1)){
		CLRPIN(SEGMENT_F_PORT, SEGMENT_F_PIN);
	} else {
		SETPIN(SEGMENT_F_PORT, SEGMENT_F_PIN);
	}
	//G
	if(bit4 || (!bit3 && bit2) || (bit2 && !bit1) || (bit3 && !bit2)){
		CLRPIN(SEGMENT_G_PORT, SEGMENT_G_PIN);
	} else {
		SETPIN(SEGMENT_G_PORT, SEGMENT_G_PIN);
	}
}
void segment_display_clear(){
	SETPIN(SEGMENT_A_PORT, SEGMENT_A_PIN);
	SETPIN(SEGMENT_B_PORT, SEGMENT_B_PIN);
	SETPIN(SEGMENT_C_PORT, SEGMENT_C_PIN);
	SETPIN(SEGMENT_D_PORT, SEGMENT_D_PIN);
	SETPIN(SEGMENT_E_PORT, SEGMENT_E_PIN);
	SETPIN(SEGMENT_F_PORT, SEGMENT_F_PIN);
	SETPIN(SEGMENT_G_PORT, SEGMENT_G_PIN);
	SETPIN(DOT_PORT, DOT_PIN);
	SETPIN(DIGIT_1_PORT, DIGIT_1_PIN);
	SETPIN(DIGIT_0_PORT, DIGIT_0_PIN);
}
